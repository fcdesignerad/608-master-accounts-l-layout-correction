<style>

	html{
		overflow: inherit;
	}

	/** Footer */
	#footer_content{
		background-color: #282828 !important;
		padding: 25px 0;
	}

	.footer{
		background-color: #282828 !important;
	}

	/** Video Gallery */

	#gallery_name{
		
		padding: 10px 20px 10px 0;
	}

	#video_gallery #video_gallery_sortable{
		display: flex;
		flex-direction: row;
		flex-wrap: wrap;
		justify-content: flex-start;
		list-style-type: none;
		width: 100%;
	}

	#video_gallery #video_gallery_sortable li{
		flex: 1;
		flex-basis: 17%;
		margin: 1%;
		flex-grow: 0;
		position: relative;
		cursor: pointer;
		box-shadow: 3px 3px 8px 0 #00000024;
   		padding: 5px;
		border: none;
		background: white;
		height: 100%;
	}

	#video_gallery .gallery-item{
		width: 100vw;
		height: 100vh;
		max-width: 210px;
		height: 200px;
		position: relative;
		overflow: hidden !important;
		margin: 0 auto;
	}

	#video_gallery #add_item{
		width: 100%;
		height: 130px;
		background-color: #58A4A0;
		position: relative;
		transition: 0.5s ease all;
	}

	#video_gallery #add_item:hover{
		background-color: #fccd56;
		cursor: pointer;
	}

	#video_gallery #add_item i{
		font-size: 3.2em;
		position: absolute;
		top: 40%;
		left: 50%;
		transform: translate(-50%);
		color: #ffffff;
	}

	#video_gallery .gallery-item img{
		width: 160px;
		height: 150px;
	}

	#video_gallery .item-information{
		display: grid;
		position: relative;
	}

	#video_gallery .item-information .item-metadata{
		display: none;
	}

	#video_gallery .item-information .item-date{
		display: inherit;
	}

	#video_gallery .item-information.open .item-metadata{
		display: inherit;
		padding: 10px 10px 30px 10px;
	}

	#video_gallery .item-title{
		max-width: 90%;
		margin: 0;
		margin-bottom: 10px;
		width: 100%;
		/* max-height: 3.5em;	 */
		height: 40px;
		color: #000000;
		font-weight: bold;
		font-size: 0.9em;
		text-align: center;
	}

	#video_gallery .item-metadata{
		text-align: initial;
		color: #656565;
		font-size: 0.85em;
		line-height: 2;
	}

	#video_gallery .item-description-wrapper{
		/* margin: 10px 0; */
		text-align: initial;
	}

	#video_gallery .item-description{
		line-height: 1.6;
	}

	#video_gallery .description-see-more, #video_gallery .description-see-less{
		text-align: initial;
		text-decoration: underline;
		color: #58A4A0;
	}

	.sm-divider{
		display: none;
		margin-top: 20px;
	}

	.grabed{
		cursor: grabbing;
	}

	#video_editor{
		position: fixed;
		width: 40%;
		top: 0;
		bottom: 0;
		right: -40%;
		transition: ease 0.5s all;
		background: #ffffff;
		z-index: 1;
		box-shadow: -1px 0 20px 0 #0000008a;
		padding-top: 35px;
	}

	#video_editor.open{
		right: 0;
	}

	#video_editor #video_form {
		width: 100%;
    	padding: 30px;
	}

	#video_editor #video_form video{
		width: 100%;
		max-height: 450px;
	}

	#main_video_placeholder {
		width: 100%;
		border: 2px dashed #5daaa7;
		height: 280px;
		border-radius: 5px !important;
		cursor: pointer;
	}

	#main_video_placeholder i {
		position: absolute;
		top: 50%;
		right: 50%;
		transform: translate(50%, -50%);
		color: #5daaa7;
		font-size: 4em;
	}

	#main_video_placeholder p {
		position: absolute;
		top: 60%;
		right: 50%;
		transform: translate(50%, -40%);
		color: #5daaa7;
		font-size: 1.6em;
		width: 100%;
		text-align: center;
	}
	
	.form-group{
		position: relative;
	}

	/** BUTTONS STYLE */

	.btn-multiplier{
		color: black;
		border: 1px #00000036 solid;
		border-radius: 5px !important;
		padding: 5px 20px;
		transition: 0.3s ease background;
		cursor: pointer;
		margin: 5px 10px;
	}

	.btn-multiplier:hover{
		background: #fcc946 !important;
		color: white;
	}

	.btn-green{
		color: white;
		background: #5daaa7 !important;
	}

	/** Truncate Text With Ellipsis  */

	.truncate-text{
		overflow: hidden;
		display: -webkit-box;
		-webkit-line-clamp: 3;
		-webkit-box-orient: vertical;
	}

	/** Loader */

	#loader {
		position: absolute;
		left: 50%;
		top: 50%;
		z-index: 1;
		margin: -75px 0 0 -75px;
		border: 5px solid #f3f3f3;
		border-radius: 50% !important;
		border-top: 5px solid #58A4A0;
		width: 100px;
		height: 100px;
		-webkit-animation: spin 2s linear infinite;
		animation: spin 2s linear infinite;
	}

	@-webkit-keyframes spin {
		0% { -webkit-transform: rotate(0deg); }
		100% { -webkit-transform: rotate(360deg); }
	}

	@keyframes spin {
		0% { transform: rotate(0deg); }
		100% { transform: rotate(360deg); }
	}

	/* Add animation to "page content" */
	.animate-bottom {
		position: relative;
		-webkit-animation-name: animatebottom;
		-webkit-animation-duration: 1s;
		animation-name: animatebottom;
		animation-duration: 1s
	}

	@-webkit-keyframes animatebottom {
		from { bottom:-100px; opacity:0 } 
		to { bottom:0px; opacity:1 }
	}

	@keyframes animatebottom { 
		from{ bottom:-100px; opacity:0 } 
		to{ bottom:0; opacity:1 }
	}

	@media(max-width: 1000px){

		#video_gallery .gallery-item {
			flex-basis: 45%;
		}			

	}

	/** Icons */

	.glyphicon-time{
		display: none !important;
	}

	.delete-item-icon, #change_video {
		position: absolute;
		right: 5px;
		top: 5px;
		color: white;
		cursor: pointer;
		text-shadow: 0px 0px 5px #000;
		z-index: 1;
	}

	.edit-item-icon{
		position: absolute;
		right: 0;
    	bottom: 15px;
		color: white;
		cursor: pointer;
		text-shadow: 0px 0px 5px #000;
		z-index: 1;	
	}

	.edit-item-icon:hover, .delete-item-icon:hover, #change_video:hover {
		color: #fcc946;
	}

	.img-zoom {
		transition: 0.5s transform ease;
	}

	.img-zoom:hover {
		transform: scale(1.1);
	}

	/** Messages  */

	.text-box{
		min-width: 220px;
		background: white;
		position: fixed;
		top: 150px;
		right: 50%;
		padding: 10px;
		border-radius: 5px !important;
		z-index: 4;
		transform: translate(50%, 50%);
		text-align: center;
		border: 1px solid #00000059;
		width: 60%;
	}

	.text-box p{
		padding: 0 10px;
	}

	.text-box i{
		font-size: 2em;
    	padding-top: 10px;
	}

	.text-box-button-wraper{
		text-align: center;
	}

	/**   */

	@media(max-width: 1050px){

		#video_gallery #video_gallery_sortable li {
			flex-basis: 22%;
		}			

	}

	@media(max-width: 770px){

		#video_gallery #video_gallery_sortable li {
			flex-basis: 30%;
		}		

		#video_editor {
			width: 55%;
			right: -55%;
		}		

	}

	@media(max-width: 500px){

		#video_gallery #video_gallery_sortable li {
			flex-basis: 90%;
		}		

		#video_editor {
			width: 65%;
			right: -65%;
		}		

	}

	@media(max-width: 380px){

		#video_gallery #video_gallery_sortable{
			padding-left: 20px;
		}

		#video_gallery #video_gallery_sortable li {
			flex-basis: 90%;
		}		

		#video_editor {
			padding-top: 100px;
			width: 95%;
			right: -95%;
		}		

		#video_gallery .gallery-item img {
			height: 140px;
		}

	}

	.isDisable{
		pointer-events: none;
	}

</style>


<div style="margin-bottom: 30px;font-size:16px" class="caption font-dark">
	<i class="icon-settings font-dark"></i>
	<?php if($authUser['role'] == 3){
		$msg = " - ".__(my_account_header_msg,[$authUser['name']]);
	}else{
		$msg = "";
	} ?>
	<span id="gallery_name" class="caption-subject bold uppercase"><?= __(my_account) ?></span>
</div>



<div class = "row" id = "video_gallery">
<form action="" method="post" id="masterform">
	<ul id = "video_gallery_sortable" <?php echo $isinvalid; ?>>
    <?php if(!empty($users)){ ?>
    <?php foreach ($users as $user){ ?>
    <li class = "ui-state-default">
        <div class = "gallery-item masters" align="center" data-id = "<?php echo $user->id; ?>">
            <?php if($user->profile_image){?>
                <img src="<?php echo HTTP_ROOT.'img/uploads/users/'.$user->profile_image;?>"  style="margin-bottom:7px;"/>
				<h4 class = "item-title truncate-text" title = "<?php echo $user->name; ?>"><?php echo $user->name; ?></h4>
            <?php }else{ ?>
                <img src="<?php echo HTTP_ROOT.'img/user_image_default.png';?>" style="margin-bottom:7px;"/>
				<h4 class = "item-title truncate-text" title = "<?php echo $user->name; ?>"><?php echo $user->name; ?></h4>
            <?php } ?>
            

        </div>
    </li>
    <?php } ?>
    <input type="hidden" name="mastersId" id="mastersId" value=""/>
    
    <?php } ?>
	</ul>
</form>
</div>

<script>
$(document).on('click','.masters',function(event){
    if($(this).attr('data-id') != 0){
        
        $('#mastersId').val($(this).attr('data-id'));
        $('#masterform').submit();
    }
 });
</script>